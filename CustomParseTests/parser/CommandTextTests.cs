﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CustomParse.parser;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomParse.parser.Tests
{
    [TestClass()]
    public class CommandTextTests
    {
        /// <summary>
        /// Testa se a leitura do comando foi feita corretamente.
        /// </summary>
        [TestMethod()]
        public void  LeituraTextoTest()
        {

            String commando = "#cmd set prj=4040 tbl=MapaOrcamentoAtualizado";
            CommandText cm = new CommandText(commando);


            Assert.AreEqual("4040", cm.Parametros["prj"]);
            Assert.AreEqual("MapaOrcamentoAtualizado", cm.Parametros["tbl"]);
            Assert.AreEqual(TipoComando.SET, cm.TipoComando);

        }


        /// <summary>
        /// Testa se a leitura do comando foi feita corretamente.
        /// </summary>
        [TestMethod()]
        public void CriacaoDeComandoTest()
        {

            String commando = "#cmd set prj=4040 tbl=MapaOrcamentoAtualizado";
            CommandText cm = new CommandText(commando);
            SetCommand s = cm.CreateCommand() as SetCommand;


            Assert.IsNotNull(s);


        }


    }
}