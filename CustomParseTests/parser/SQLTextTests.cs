﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CustomParse.parser;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomParse.parser.Tests
{
    [TestClass()]
    public class SQLTextTests
    {

        private const String LINHA_INICIAL = "'Texto gerado automáticamente\r\n";

        /// <summary>
        /// Testa se há uma consulta válida
        /// </summary>
        [TestMethod()]
        public void CriarInstanciaTest()
        {
            String input = "SELECT * FROM ";
            Assert.IsNotNull(SQLText.Parse(input));

        }

        /// <summary>
        /// Testa se há uma consulta válida com select
        /// </summary>
        [TestMethod()]
        public void TextoValidoComSelect()
        {
            String input = "SELECT * FROM ";
            bool esperado = true;

            bool resultado = SQLText.IsValid(input);

            Assert.IsTrue(esperado.Equals(resultado));
        }


        /// <summary>
        /// Testa se há uma consulta válida com INSERT
        /// </summary>
        [TestMethod()]
        public void TextoValidoComInsert()
        {
            String input = "INSERT INTO TableA(x,y,z)";
            bool esperado = true;

            bool resultado = SQLText.IsValid(input);

            Assert.IsTrue(esperado.Equals(resultado));
        }


        /// <summary>
        /// Testa se há uma consulta válida com Delete
        /// </summary>
        [TestMethod()]
        public void TextoValidoComDelete()
        {
            String input = "DELETE FROM TABELA";
            bool esperado = true;

            bool resultado = SQLText.IsValid(input);

            Assert.IsTrue(esperado.Equals(resultado));
        }


        /// <summary>
        /// Testa se há uma consulta válida com Update
        /// </summary>
        [TestMethod()]
        public void TextoValidoComUpdate()
        {
            String input = "UPDATE TABELA SET";
            bool esperado = true;

            bool resultado = SQLText.IsValid(input);

            Assert.IsTrue(esperado.Equals(resultado));
        }


        /// <summary>
        /// Testa se há uma consulta válida com View
        /// </summary>
        [TestMethod()]
        public void TextoValidoComView()
        {
            String input = "CREATE VIEW";
            bool esperado = true;

            bool resultado = SQLText.IsValid(input);

            Assert.IsTrue(esperado.Equals(resultado));
        }

        /// <summary>
        /// Testa se há uma consulta válida com From
        /// </summary>
        [TestMethod()]
        public void TextoValidoComFrom()
        {
            String input = "UPDATE TABLE A FROM TABLE B";
            bool esperado = true;

            bool resultado = SQLText.IsValid(input);

            Assert.IsTrue(esperado.Equals(resultado));
        }

        /// <summary>
        /// Testa se o texto sql convertido para o vb é inválido
        /// </summary>        
        [TestMethod()]
        public void textoVBValidoTest()
        {
            String input = "SELECT * FROM TABELA";
            SQLText obj = ParseUtil.getParser(input) as SQLText;
            String resultado = LINHA_INICIAL + obj.getTextoVB();

            Assert.IsTrue(VBText.IsValid(resultado));
        }

        /// <summary>
        /// Testa se fez a quebra corretamente do texto para copiar no vb
        /// </summary>
        [TestMethod()]
        public void textoVBTamanhoInvalido()
        {
            String input = "SELECT * FROM TABLE\r\n" +
                           "INNER JOIN TABLE B\r\n" +
                           "INNER JOIN TABLE B\r\n" +
                           "INNER JOIN TABLE B\r\n" +
                           "INNER JOIN TABLE B\r\n" +
                           "INNER JOIN TABLE B\r\n" +
                           "INNER JOIN TABLE B\r\n" +
                           "INNER JOIN TABLE B\r\n" +
                           "INNER JOIN TABLE B\r\n" +
                           "INNER JOIN TABLE B\r\n" +
                           "INNER JOIN TABLE B\r\n" +
                           "INNER JOIN TABLE B\r\n" +
                           "INNER JOIN TABLE B\r\n" +
                           "INNER JOIN TABLE B\r\n" +
                           "INNER JOIN TABLE B\r\n" +
                           "INNER JOIN TABLE B\r\n" +
                           "INNER JOIN TABLE B\r\n" +
                           "INNER JOIN TABLE B\r\n" +
                           "INNER JOIN TABLE B\r\n" +
                           "INNER JOIN TABLE B";

            SQLText obj = ParseUtil.getParser(input) as SQLText;

            String[] lines = obj.getTextoVB().Split("VbCrLF");
            Assert.IsTrue(lines.Length > 20);


        }

        /// <summary>
        /// Testa se o resultado é um texto do vb
        /// </summary>
        [TestMethod()]
        public void textoVBFormatoInvalido()
        {
            String input = "SELECT * FROM TABELA";

            String resultado = input;
            Assert.IsFalse(resultado.Contains("VbCrLF"));
            Assert.IsFalse(resultado.Contains("&"));
            Assert.IsFalse(resultado.Contains("_"));
            Assert.IsFalse(resultado.Contains("strSql"));
            Assert.IsFalse(resultado.Contains("\""));
        }





    }
}