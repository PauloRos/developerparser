﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CustomParse.parser;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomParse.parser.Tests
{
    [TestClass()]
    public class VBTextTests
    {

        private static String text = "strSql = \"Foram exportados \" & registros & \" para o arquivo texto\"";
        private const String LINHA_INICIAL = "--Texto convertido automáticamente\r\n";


        /// <summary>
        /// Retorn o texto sem caracteres de controle ex: \n, \r
        /// </summary>
        /// <returns></returns>
        private String RetornaTextoLimpo(String t) {

            return t.Replace("\r\n", String.Empty).Replace("\"",String.Empty).Replace("&","");
        
        }

        [TestMethod()]
        public void ParseTest()
        {

            IDevParser p = VBText.Parse(text);
            Assert.IsNotNull(p);
        }

        [TestMethod()]
        public void getTextTest()
        {
            IDevParser p = VBText.Parse(text);
            Assert.AreEqual(p.getText(), text) ;
        }

        [TestMethod()]
        public void IsValidTest()
        {
            Assert.IsTrue(VBText.IsValid(text));
        }
        
        /// <summary>
        /// Testa se não considerou o teste de igualdade como atribuição e portanto não removeu os mesmos
        /// </summary>
        [TestMethod()]
        public void RemoverAtribuicoesFalsoPositivoTest()
        {

            String input = "WHERE 1 = 0 AND A.B = C.X&";
            String esperado = RetornaTextoLimpo("WHERE 1 = 0 AND A.B = C.X");
            
            VBText vt = VBText.Parse(input) as VBText;
            string resultado = RetornaTextoLimpo(vt.RemoverAtribuicoes(input));


            Assert.IsTrue(esperado.Equals(resultado));

        }
        /// <summary>
        /// Testa se removeu as atribuições que não estão no começo da linha e nem seguidas do vbcrlf
             /// </summary>
        [TestMethod()]
        public void RemoverAtribuicoesSemQubraDeLinha()
        {

            String input = "\"WHERE 1 = 0 AND A.B = C.X\" strsql = strsql &_ ";
            String esperado = RetornaTextoLimpo("\"WHERE 1 = 0 AND A.B = C.X\"");
            
            VBText vt = VBText.Parse(input) as VBText;
            string resultado = RetornaTextoLimpo(vt.RemoverAtribuicoes(input));
            
            Assert.IsTrue(esperado.Equals(resultado));

        }

        /// <summary>
        /// Testa se as variáveis de atribuição foram removidas do texto da consulta sql
        /// Pois elas não fazem sentido em uma consulta sql
        /// </summary>
        [TestMethod()]
        public void RemoverAtribuicoesTest()
        {
            
            VBText p = VBText.Parse("strvar = strvar &  \"Foram exportados \" para o arquivo texto\"") as VBText;
            String resultado = "Foram exportados  para o arquivo texto";
            String textoLimpo = RetornaTextoLimpo(p.RemoverAtribuicoes(p.getText()));
            Assert.IsTrue(resultado.Equals(textoLimpo));
        }


        /// <summary>
        /// Testa a substituição das variáveis concatenadas na consutla sql por parametros sql
        /// </summary>
        [TestMethod()]
        public void SubstituiParametrosTest()
        {
            HashSet<String> list = new HashSet<string>();
            VBText p = VBText.Parse("\"string consulta vb\" & strVar &\"") as VBText;
            
            String resultado = "string consulta vb @_strVar_";


            String textoLimpo = RetornaTextoLimpo(p.SubstituirParametros(p.getText(), list));
           
            Assert.IsTrue(resultado.Equals(textoLimpo));
        }


        /// <summary>
        /// Testa a substituição das variáveis concatenadas na consutla sql por parametros sql
        /// </summary>
        [TestMethod()]
        public void SubstituiParametrosFalsoPositivoTest()
        {
            String input = "            \"FROM Recebidas  \" & vbCrLf\r\n\r\n   " +
                                        "strSQL = strSQL & _\r\n              " +
                                        "\"LEFT JOIN ( \" & vbCrLf _\r\n            & \"                               " +
                                        "SELECT Empresa_rpde, \" & vbCrLf _\r\n            & \"" +
                                        "                                      Obra_rpde, \" & vbCrLf _\r\n            & \"" +
                                        "                                      NumVend_rpde, \" & vbCrLf _\r\n            & \"" +
                                        "                                      NumParc_rpde, \" & vbCrLf _\r\n            & \"";


            HashSet < String > list = new HashSet<string>();
            VBText vt = VBText.Parse(input) as VBText;
            string resultado = vt.SubstituirParametros(input,list);


            Assert.IsFalse(resultado.Contains("@"));

        }


        /// <summary>
        /// Testa a substituição das chamada de método concatenadas na consutla sql por parametros
        /// </summary>
        [TestMethod()]
        public void SubstituirParamestrosDoTipoFuncaoTest()
        {

            HashSet<String> list = new HashSet<string>();
            VBText p = VBText.Parse("\"string consulta vb\" & IIF(ISNULL(Year(strDate)), \"NULL\", strdia + strmes  ) & \"\"") as VBText;

            String resultado = "string consulta vb @_IIF_ISNULL_Year_strDate_NULL_strdia_strmes_ ";
 
            String textoLimpo = RetornaTextoLimpo(p.SubstituirParametros(p.getText(), list));

            Assert.IsTrue(resultado.Equals(textoLimpo));
        }


        /// <summary>
        /// Testa a substituição das chamada de método concatenadas na consutla sql por parametros
        /// </summary>
        [TestMethod()]
        public void SubstituirParametrosNoFinalDaLinha()
        {

            HashSet<String> list = new HashSet<string>();
            VBText p = VBText.Parse("\"string consulta vb\" & strVar") as VBText;

            String resultado = "string consulta vb @_strVar_";

            String textoLimpo = RetornaTextoLimpo(p.SubstituirParametros(p.getText(), list));

            Assert.IsTrue(resultado.Equals(textoLimpo));
        }


        /// <summary>
        /// Testa a substituição das chamada de método concatenadas na consutla sql por parametros
        /// </summary>
        [TestMethod()]
        public void SubstituirParametrosMaisDeUM()
        {

            HashSet<String> list = new HashSet<string>();
            VBText p = VBText.Parse("\"string consulta \" & registros & \" \" & registros2 & \"") as VBText;

            String resultado = "string consulta  @_registros_   @_registros2_ ";

            String textoLimpo = RetornaTextoLimpo(p.SubstituirParametros(p.getText(), list));

            Assert.IsTrue(resultado.Equals(textoLimpo));
        }


        /// <summary>
        /// Testa se o texto sql retornado do objeto é coerente com uma consulta sql
        /// </summary>
        [TestMethod()]
        public void getSQLTextTest()
        {


            String input = "strSql = \"Foram exportados \" & registros & \" para o arquivo texto\"";
            String esperado = RetornaTextoLimpo(LINHA_INICIAL) + "DECLARE @_registros_ As VARCHAR(800);SET @_registros_  = '';Foram exportados  @_registros_  para o arquivo texto";


            VBText p = VBText.Parse(input) as VBText;
            
            String resultado =RetornaTextoLimpo( p.getSqlText());
            Assert.IsTrue(resultado.Equals(esperado));




        }
        

    }
}