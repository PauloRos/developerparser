﻿using System;
using System.Collections.Generic;
using System.Text;
using Data;
using System.Configuration;
using System.IO;

namespace Business
{
    public class TesteApi : ITesteAPI
    {

        private List<Dictionary<String, object>> _beforeValues = new List<Dictionary<string, object>>();
        private List<Dictionary<String, object>> _afterValues = new List<Dictionary<string, object>>();

        private readonly string _conString = @"Data Source=DEV-PAULOR-NB\MSSQL2019; User Id=usruau; password=123; Initial Catalog=NoteBook_1006";

        public List<Dictionary<string, object>> GetAfterValues()
        {
            return _afterValues;
        }

        public List<Dictionary<string, object>> GetBeforeValues()
        {
            return _beforeValues;
        }

        /// <summary>
        /// Replica as tabelas para geração do teste.
        /// </summary>
        /// <param name="NumeroProjeto"></param>
        /// <param name="tabelas"></param>
        public void PrepararAmbiente(int NumeroProjeto, List<string> tabelas)
        {
            foreach (var item in tabelas)
            {

                IDataSource<ILocalizabeObject<String, object>> inds = new SQLServerDataSource(item, _conString);
                IDataSource<ILocalizabeObject<String, object>> outds = new XMLSerializableDataSource<Dictionary<string, object>>($"test_{NumeroProjeto.ToString()}_{item}.xml");
                outds.CommitAll(inds.GetALL());
            }
        }

        /// <summary>
        /// Compara os valores atuais com os replicados e detecta alterações.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="inputDataSouce"></param>
        /// <param name="testDataSource"></param>
        public void ComparaValores(int key, IDataSource<Dictionary<string, object>> inputDataSouce, IDataSource<Dictionary<string, object>> testDataSource)
        {
            String[] files = Directory.GetFiles(Directory.GetCurrentDirectory());
            List<String> compareFiles = new List<string>();
            foreach (String item in files)
            {
                if (item.Contains($"test_{key.ToString()}", StringComparison.OrdinalIgnoreCase))
                {
                    compareFiles.Add(item);
                }
                
            }
            foreach (String item in compareFiles)
            {
                IDataSource<Dictionary<string, object>> test = new XMLSerializableDataSource<Dictionary<string, object>> (item);
                String tabela = item.Replace($"test_{key =}_", "").Replace(".xml", "");
                IDataSource<Dictionary<string, object>> inputds = new SQLServerDataSource(tabela,_conString);

                var testeData = test.GetALL();
                var inputData = inputds.GetALL();

                //Para cada registro no banco
                foreach (var line in inputData)
                {
                    //para cada coluna
                    foreach (var col in line)
                    {
                       // line.
                    }
                }

            }

        }
    }
}
