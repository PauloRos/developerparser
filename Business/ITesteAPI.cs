﻿using System;
using System.Collections.Generic;
using Data;

namespace Business
{
    public interface ITesteAPI
    {
        /// <summary>
        /// Prepara o ambiente para um projeto a partir das tabelas do bd
        /// </summary>
        /// <param name="NumeroProjeto"></param>
        /// <param name="tabelas"></param>
        public void PrepararAmbiente(int key, List<String> tabelas);
        /// <summary>
        /// Retorna uma coleção com as colunas que tiveram diferença  
        /// </summary>
        /// <returns></returns>
        public List<ILocalizabeObject<string, object>> GetBeforeValues();

        /// <summary>
        /// Retorna uma coleção com as colunas diferentes 
        /// </summary>
        public List<ILocalizabeObject<string, object>> GetAfterValues();

        /// <summary>
        /// Compara os valores da fonte de entrada com a fonte de dados de teste setada anteriormente.
        /// </summary>
        /// <param name="inputDataSouce"></param>
        /// <param name="testDataSource"></param>
        public void ComparaValores( int key, IDataSource<ILocalizabeObject<string,object>> inputDataSouce, IDataSource<ILocalizabeObject<string, object>> testDataSource);

    }
}
