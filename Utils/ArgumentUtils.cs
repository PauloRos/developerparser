﻿using System;

namespace Utils
{
    public static class ArgumentUtils
    {
        /// <summary>
        /// Valida os argumentos usados
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="arg"></param>
        /// <param name="argName"></param>
        public static void ThrowIFInvalid<T>(T arg, String argName)
        {

            switch (typeof(T).ToString().ToLower())
            {
                case "string":
                    {
                        if (String.IsNullOrEmpty(arg as string))
                        {
                            throw new ArgumentException(String.Format($"Argumento {argName} nulo ou vazio."));
                        }

                        break;
                    }
                default:
                    {
                        if (arg == null)
                        {
                            throw new ArgumentNullException(String.Format($"Argumetno {argName} nulo."));

                        }

                    }

                    break;
            }

        }
    }
}
