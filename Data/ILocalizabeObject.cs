﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public interface ILocalizabeObject<K,D>
    {

        /// <summary>
        /// Retorna o hash do objeto para ser localizado nas coleções
        /// </summary>
        /// <returns></returns>
        public long GetIdHash();

        /// <summary>
        /// Retorna o meta data do objeto
        /// </summary>
        /// <returns></returns>
        public Dictionary<K, D> GetMetaData();


    }
}
