﻿using System;
using System.Collections.Generic;

namespace Data
{
    public interface IDataSource<T>
    {

        public void Add(T value);

        public T Remove(T value);

        public T Update(T value, T old);

        public T Get(T key);

        public IEnumerable<T> GetALL();

        public void CommitAll(IEnumerable<T> all);

    
    }
}
