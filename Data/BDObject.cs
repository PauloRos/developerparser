﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    class BDObject : ILocalizabeObject<String, object>
    {
        private long _hash;
        private IDictionary<string, object> _metaData;


        public BDObject(Dictionary<String,object> metadata, long hash)
        {
            _metaData = metadata;
            _hash = hash;
        }

        /// <summary>
        /// Retorna o hash gerado a partir da chave primaria
        /// </summary>
        /// <returns></returns>
        public long GetIdHash()
        {
            return _hash;
        }

        /// <summary>
        /// Retorna os metaDados da tabela
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> GetMetaData()
        {
            return GetMetaData();
        }
    }
}
