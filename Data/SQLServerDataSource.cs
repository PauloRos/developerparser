﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Data
{
    public class SQLServerDataSource : IDataSource<ILocalizabeObject<String, Object>>
    {

        private List<String> _definitionList = new List<string>();
        private List<String> _keys = new List<string>();
        private String _tabela;
        private String _connectionString;


        public SQLServerDataSource(String tabela, String connectionString)
        {
            _tabela = tabela;
            _connectionString = connectionString;
            FillTableDefinition();
            FillPrimaryKeyDefinition();

        }


        /// <summary>
        /// Gera o hash da composição de chave primaria
        /// </summary>
        /// <param name="metadata"></param>
        /// <returns></returns>
        public long GetHash(Dictionary<String, object> metadata)
        {

            long hash = 0;
            foreach (var item in _keys)
            {
                hash += metadata[item].GetHashCode();
            }
            return hash;

        }

        /// <summary>
        /// Preenche as colunas que compoem a chave da tabela
        /// </summary>
        private void FillPrimaryKeyDefinition()
        {


            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();

                SqlCommand com = new SqlCommand($"sp_pkeys @table_owner = 'dbo', @table_name = '{_tabela}'", con);

                SqlDataReader reader = com.ExecuteReader();
                while (reader.Read())
                {
                    _keys.Add(reader.GetValue(3).ToString());
                }
                reader.Close();
            }


        }


        /// <summary>
        /// Preenche as colunas que compoem a chave da tabela
        /// </summary>
        private void FillTableDefinition()
        {


            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();

                SqlCommand com = new SqlCommand($"SELECT * FROM sys.columns WHERE object_id = object_id('{_tabela}')", con);

                SqlDataReader reader = com.ExecuteReader();
                //Pegando a definição da tabela
                while (reader.Read())
                {
                    _definitionList.Add(reader.GetString(1));
                }

                reader.Close();
            
            }






        }

        public void Add(ILocalizabeObject<String, Object> value)
        {

            List<ILocalizabeObject<String, Object>> _list = new List<ILocalizabeObject<String, Object>>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();

                StringBuilder sb = new StringBuilder();
                StringBuilder sbValues = new StringBuilder();
                sb.AppendLine($"INSERT INTO { _tabela}( ");

                foreach (var item in value.GetMetaData())
                {
                    sb.AppendLine($",{item.Key } ");
                    sbValues.AppendLine($" ,@{item.Key} ");
                }

                SqlCommand com = new SqlCommand(Regex.Replace(sb.ToString(), @",{1}", "") + ") VALUES (" + Regex.Replace(sbValues.ToString(), @",{1}", "") + ")", con);

                com.Parameters.Clear();
                foreach (var item in value.GetMetaData())
                {
                    com.Parameters.AddWithValue($"@{item.Key}", item.Value);
                }


                int quantidade = com.ExecuteNonQuery();

                if (quantidade > 0)
                {
                    _list.Add(value);
                }



            }


        }

        public ILocalizabeObject<String, Object> Get(ILocalizabeObject<String, Object> key)
        {

            List<ILocalizabeObject<String, Object>> _list = new List<ILocalizabeObject<String, Object>>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();

                
                StringBuilder sbwhere = new StringBuilder();
                StringBuilder sb = new StringBuilder();
                sb.AppendLine($"SELECT * FROM {_tabela}");
                foreach (var item in key.GetMetaData())
                {
                    sbwhere.AppendLine($"AND { item.Key } = @{ item.Key} ");
                }

                SqlCommand com = new SqlCommand(sb.ToString() + " WHERE " + Regex.Replace(sbwhere.ToString(), @"(AND){1}", ""), con);

                com.Parameters.Clear();
                foreach (var item in key.GetMetaData())
                {
                    com.Parameters.AddWithValue($"@{item.Key}", item.Value);
                }


                SqlDataReader reader = com.ExecuteReader();
                //Pegando dados da tabela
                while (reader.Read())
                {
                    Dictionary<String, Object> dados = new Dictionary<string, object>();
                    for (int i = 0; i < _definitionList.Count; i++)
                    {
                        dados.Add(_definitionList[i], (reader.GetValue(reader.GetOrdinal(_definitionList[i]))));
                    }
                    _list.Add(new BDObject(dados, GetHash(dados)));

                }
                reader.Close();

            }
            return _list.Count == 0 ? null : _list[0];
        }

        public IEnumerable<ILocalizabeObject<String, Object>> GetALL()
        {
            List<ILocalizabeObject<String, Object>> _list = new List<ILocalizabeObject<String, Object>>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();

                SqlCommand com = new SqlCommand($"SELECT * FROM sys.columns WHERE object_id = object_id('{_tabela}')", con);

                SqlDataReader reader = com.ExecuteReader();
                //Pegando a definição da tabela
                while (reader.Read())
                {
                    _definitionList.Add(reader.GetString(1));
                }

                reader.Close();
                com = new SqlCommand($"SELECT * FROM {_tabela}", con);

                reader = com.ExecuteReader();
                //Pegando dados dat tabela
                while (reader.Read())
                {
                    Dictionary<String, Object> dados = new Dictionary<string, object>();
                    for (int i = 0; i < _definitionList.Count; i++)
                    {
                        dados.Add(_definitionList[i], (reader.GetValue(reader.GetOrdinal(_definitionList[i]))));
                    }
                    _list.Add(new BDObject(dados,GetHash(dados)));

                }
                reader.Close();

            }
            return _list;
        }

        public ILocalizabeObject<String, Object> Remove(ILocalizabeObject<String, Object> value)
        {


            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();

                StringBuilder sb = new StringBuilder();
                StringBuilder sbWhere = new StringBuilder();
                sb.AppendLine($"DELETE FROM { _tabela} ");

                foreach (var item in value.GetMetaData())
                {
                    sbWhere.AppendLine($"AND { item.Key } = @{ item.Key} ");
                }

                SqlCommand com = new SqlCommand(sb.ToString() + " WHERE " + Regex.Replace(sbWhere.ToString(), @"(AND){1}", ""), con);

                com.Parameters.Clear();
                foreach (var item in value.GetMetaData())
                {
                    com.Parameters.AddWithValue($"@{item.Key}", item.Value);
                }

                int quantidade = com.ExecuteNonQuery();

                if (quantidade > 0)
                {
                    return value;
                }



            }

            return null;

        }

        public ILocalizabeObject<String, Object> Update(ILocalizabeObject<String, Object> value, ILocalizabeObject<String, Object> old)
        {


            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();

                StringBuilder sb = new StringBuilder();
                StringBuilder sbWhere = new StringBuilder();
                sb.AppendLine($"UPDATE { _tabela} SET ");

                foreach (var item in value.GetMetaData())
                {


                    sb.AppendLine($",{item.Key } = @{item.Key} ");
                    sbWhere.AppendLine($",{ item.Key } = @old{ item.Key} ");
                }

                SqlCommand com = new SqlCommand(Regex.Replace(sb.ToString(), @",{1}", "") + " WHERE " + Regex.Replace(sbWhere.ToString(), @",{1}", ""), con);

                com.Parameters.Clear();
                foreach (var item in value.GetMetaData())
                {
                    com.Parameters.AddWithValue($"@{item.Key}", item.Value);
                    com.Parameters.AddWithValue($"@old{item.Key}", old.GetMetaData()[item.Key.ToString()]);
                }

                int quantidade = com.ExecuteNonQuery();

                if (quantidade > 0)
                {
                    return value;
                }


            }
            return null;
        }

        public void CommitAll(IEnumerable<ILocalizabeObject<String, Object>> all)
        {
            foreach (var item in all)
            {
                Add(item);
            }


        }



    }

}
