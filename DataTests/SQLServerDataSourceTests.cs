﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Data.Tests
{
    [TestClass()]
    public class SQLServerDataSourceTests
    {
        private String cons = @"Data Source=DEV-PAULOR-NB\MSSQL2019; User Id=usruau; password=123; Initial Catalog=NoteBook_1006";

        [TestMethod()]
        public void SQLServerDataSourceTest()
        {
            SQLServerDataSource s = new SQLServerDataSource("Pessoas",cons);
            Assert.IsTrue(s != null);
        }

        [TestMethod()]
        public void AddTest()
        {
            SQLServerDataSource s = new SQLServerDataSource("TabelaTeste", cons);            
            Dictionary<String, Object> dic = new Dictionary<string, object>();
            dic.Add("campo", "a");
            s.Add(dic);
            Assert.IsTrue(s.GetALL().GetEnumerator().MoveNext());
        }

        [TestMethod()]
        public void GetTest()
        {
            SQLServerDataSource s = new SQLServerDataSource("TabelaTeste", cons);
            Dictionary<String, Object> dic = new Dictionary<string, object>();
            dic.Add("campo", "fer");
            Assert.IsTrue(s.Get(dic)!=null);
        }

        [TestMethod()]
        public void GetALLTest()
        {
            SQLServerDataSource s = new SQLServerDataSource("Pessoas", cons);
            Assert.IsTrue(s.GetALL().GetEnumerator().MoveNext());
        }

        [TestMethod()]
        public void RemoveTest()
        {
            SQLServerDataSource s = new SQLServerDataSource("TabelaTeste", cons);
            Dictionary<String, Object> dic = new Dictionary<string, object>();
            dic.Add("campo", "a");
            s.Remove(dic);
            Assert.IsTrue(s.GetALL().GetEnumerator().MoveNext());
        }

        [TestMethod()]
        public void UpdateTest()
        {
            SQLServerDataSource s = new SQLServerDataSource("TabelaTeste", cons);
            Dictionary<String, Object> newdic = new Dictionary<string, object>();
            newdic.Add("campo", "as");
            Dictionary<String, Object> dic = new Dictionary<string, object>();
            dic.Add("campo", "ar");

            Assert.IsTrue(s.Update(newdic, dic) != null);
            Assert.IsTrue(s.Update(dic, newdic) != null);

        }

        [TestMethod()]
        public void CommitAllTest()
        {
            SQLServerDataSource s = new SQLServerDataSource("TabelaTeste", cons);
            
            Dictionary<String, Object> newdic = new Dictionary<string, object>();
            newdic.Add("campo", "a");
            
            Dictionary<String, Object> dic = new Dictionary<string, object>();
            dic.Add("campo", "b");

            s.CommitAll(new List<Dictionary<String, Object>>() { dic, newdic });

        }

   
    }
}