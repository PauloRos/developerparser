﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomParse.parser
{
    public interface ITextParser<T>
    {

        T Parse(String text);
    }
}
