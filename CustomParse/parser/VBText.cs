﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;
using System.Diagnostics;

namespace CustomParse.parser
{
    public class VBText : IDevParser
    {
        private String _text = "";
        private String _vbText = "";
        private DevFormat TipoConversao;
        public static String Pattern { get { return "((v|V)(b|B)(c|C)(r|R)(l|L)(f|F))+| _|&|\""; } }

        /// <summary>
        /// Cria uma instância de IDevParser a partir de uma string
        /// </summary>
        /// <param name="text"> String a ser convertida </param>
        /// <returns></returns>
        public static IDevParser Parse(String text)
        {

            ParseUtil.ValidarArgumentosParse(text);


            if (VBText.IsValid(text))
            {
                VBText cv = new VBText();
                cv._text = text;
                cv._vbText = text;
                cv.DefinirConversao(DevFormat.CONSULTA_SQL);
                return cv;
            }
            else
            {
                return null;
            }


        }


        private String RetornTipoParametroSql(String parametro)
        {

            if (parametro.ToLower().Contains("str"))
            {
                return "VARCHAR(800)";
            }
            else if (parametro.ToLower().Contains("int"))
            {
                return "INTEGER";
            }
            else if (parametro.ToLower().Contains("dt"))
            {
                return "DATETIME";
            }
            else if (parametro.ToLower().Contains("dbl"))
            {
                return "NUMERIC(18,6)";
            }
            else
            {
                return "VARCHAR(800)";
            }


        }

        /// <summary>
        /// Retorna a string original do vb
        /// </summary>
        /// <returns></returns>
        public string getText()
        {

            return _vbText;
        }

        /// <summary>
        /// Retorna uma string para ser usado no SqlServer
        /// </summary>
        /// <returns></returns>
        public string getSqlText()
        {
            StringBuilder resultado;
            StringBuilder sbParametros;
            HashSet<String> parametros = new HashSet<string>();

            resultado = this.RemoveCaracteresEspeciais(parametros);
            sbParametros = this.AdicionarTextoAParametros(parametros);

            return "--Texto convertido automáticamente\r\n" + sbParametros.ToString() + resultado.ToString();
        }


        /// <summary>
        /// Remove as atribuições no meio da consulta
        ///     Ex: strs = "Select"
        /// </summary>
        public String RemoverAtribuicoes(String linha)
        {


            if (linha.Length > 0)
            {


                try
                {
                    String newLine = Regex.Replace(linha, "\".+\"", "#newLine");
                    String[] textos = newLine.Split("#newLine");

                    if (!newLine.Equals(linha))
                    {
                        foreach (String l in textos)
                        {
                            if (l.Contains("="))
                            {
                                linha = linha.Replace(l, String.Empty);
                            }
                        }
                    }

                }
                catch (Exception e)
                {
                    throw new ArgumentException($"Erro na linha { linha }", e.InnerException);
                }
            }


            //Remove tudo que não esteja entre aspas simples
            return linha;

        }

        /// <summary>
        /// Substitui os parâmetros concatenados na string do vb
        /// </summary>
        /// <param name="line"></param>
        /// <param name="parametros"></param>
        /// <returns></returns>

        public String SubstituirParametros(String line, HashSet<String> parametros)
        {

            String[] patterns = { "(&.+?&)+" };

            String tempLine;
            // Se a linha não terminar com & comercial, adiciona um 
            if (!line.EndsWith("&"))
            {
                //trato a linha para achar os parametros de fim de linha
                line = line + "&";
                
            }
            tempLine = line;
            tempLine = tempLine.Replace("\r\n", " & _");

            foreach (String pat in patterns)
            {
                String[] pars = Regex.Replace(tempLine, pat, "#param").Split("#param");
                
                //Marcando conteudo
                foreach (String item in pars)
                {
                    if (!String.IsNullOrEmpty(item) )
                    {
                        tempLine = tempLine.Replace(item, "#conteudo");

                    }

                }


                //marco o conteudo, depois crio linhas que não sejam de conteudo
                //Tudo que estiver concatenado no meio do texto será considerado como parametro sql
                String[] parametrosLinha = tempLine.Split("#conteudo");
                
                foreach (String par in parametrosLinha)
                {
                    if (!String.IsNullOrEmpty(par) && Regex.IsMatch(par.Replace(" ", String.Empty), "(&[a-z|A-Z|0-9|\\(|\\)|\\+|\\-|\\*|\\/|\"|,]+&)+"))
                    {
                        //faço um flag de substituição
                        line = line.Replace(par, "#newParameter").Replace("\"","");

                        //tratando o parametro
                        String sqlParameter = $"@{Regex.Replace(par.Replace(" ",String.Empty), @"[^a-z|A-Z|0-9]+", "_")}";

                        //adiciono o parametro tratado para ser usado no sql
                        parametros.Add(sqlParameter);

                        //substitui o flag pelo parametro tratado
                        line = line.Replace("#newParameter", sqlParameter).Replace("\"", "");


                    }


                }

            }            


            return line;

        }


        public StringBuilder AdicionarTextoAParametros(HashSet<String> parametros)
        {

            StringBuilder builder = new StringBuilder();

            //Escrevendo os parâmetros do texto
            foreach (String s in parametros)
            {

                builder.AppendLine($"DECLARE {s} As {RetornTipoParametroSql(s)};");

                if (s.ToLower().StartsWith("str"))
                {
                    builder.AppendLine($"SET {s}  = '';") ;
                }
                else if (s.ToLower().StartsWith("int"))
                {
                    builder.AppendLine($"SET {s}  = 0;");
                }
                else if (s.ToLower().StartsWith("dt"))
                {
                    builder.AppendLine($"SET {s}  = '01/01/1990';");
                }
                else if (s.ToLower().StartsWith("dbl"))
                {
                    builder.AppendLine($"SET {s}  = 0.0;");
                }
                else
                {
                    builder.AppendLine($"SET {s}  = '';"); ;
                }
                

            }

            return builder;

        }


        /// <summary>
        /// Remove todos os caracteres especias no texto do vb
        ///     Ex: strs = "Select"
        /// </summary>
        private StringBuilder RemoveCaracteresEspeciais(HashSet<String> parametros)
        {

            StringBuilder b = new StringBuilder();
            String[] lines;


            //Procurando por vbcrlf
            _text = Regex.Replace(_text, @"((v|V)(b|B)(c|C)(r|R)(l|L)(f|F))+", "#New");
            lines = Regex.Split(_text, "#New");


            foreach (String s in lines)
            {
                //Retirando as strings de atribuição
                String temp = RemoverAtribuicoes(s);

                //Retirando os parâmetros no meio da consulta ou instrução sql
                temp = SubstituirParametros(temp, parametros);

                //Retirando os caracteres de controle do vb
                temp = Regex.Replace(temp, "(& )", String.Empty);
                temp = Regex.Replace(temp, "(&)", String.Empty);
                temp = Regex.Replace(temp, "( _)", String.Empty);
                temp = Regex.Replace(temp, "(\"')", String.Empty);
                temp = Regex.Replace(temp, "('\")", String.Empty);
                temp = Regex.Replace(temp, "(\")", String.Empty);

                b.Append(temp);
            }

            return b;


        }


        /// <summary>
        /// Testa se o valor tem a possibilidade de ser convertido em um texto de consulta a partir do texto do VB 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool IsValid(String text)
        {

            Regex r = new Regex(VBText.Pattern);
            return r.IsMatch(text);

        }

        public void DefinirConversao(DevFormat tipo)
        {
            this.TipoConversao = tipo;
        }

        public string RetornaTextoConvertido()
        {

            switch (TipoConversao)
            {
                case DevFormat.CONSULTA_SQL:
                    {
                        return getSqlText();
                    }
                case DevFormat.CONSULTA_VB:
                    {

                        return getText();
                    }

                default:
                    {
                        return getSqlText();

                    }
            }

        }

    }
}