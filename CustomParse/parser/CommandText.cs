﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Utils;

namespace CustomParse.parser
{
    public class CommandText 
    {

        private string _text;
        private Dictionary<String, String> _parametros = new Dictionary<string, string>();
        private TipoComando _tipoComando;

        public string Text { get => _text; private set => _text = value; }
        public Dictionary<String, String> Parametros { get => _parametros; private set => _parametros = value; }
        public TipoComando TipoComando { get => _tipoComando; private set => _tipoComando = value; }



        public CommandText(String text)
        {
            ArgumentUtils.ThrowIFInvalid<string>(text, "texto");
            _text = text;
            LeArgumentosDoComando();
            LeTipoDeComando();

        }

        /// <summary>
        /// Converte um texo para um commando na api
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public ICommand CreateCommand()
        {
          


            switch (TipoComando)
            {
                case TipoComando.SET:
                    return new SetCommand(this);
                case TipoComando.RUN:
                    return null;
                case TipoComando.RESET:
                    return null;
                default:
                    return null;
                 
            }


        }



        /// <summary>
        ///  Testa se tem comando para ser convetido no texto
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool TemComando(string text) {

            ArgumentUtils.ThrowIFInvalid<string>(text, "texto");

            return text.Contains("#cmd");
        }

        /// <summary>
        /// Le os argumentos do comando
        /// </summary>
        private void LeArgumentosDoComando() 
        {
            String[] lines = _text.Split(" ");
    
            foreach (var item in lines)
            {
                if (item.Contains("="))
                {
                    String[] values = item.Split("=");

                    //Contem palavra
                    if (Regex.IsMatch(values[0], @"\w+") && Regex.IsMatch(values[1], @"\w+"))
                    {
                        _parametros.Add(Regex.Match(values[0], @"\w+").Value, Regex.Match(values[1], @"\w+").Value);

                    }
                    else {

                        throw new ArgumentException($"Os valores {values[0]} e {values[1]} não correspodem a um parametro de comando válido.");
                    }

                    
                }

            }
            
        }

        /// <summary>
        /// Le o texto dos comando
        /// </summary>
        private void LeTipoDeComando()
        {
            switch (_text.ToLower())
            {
                case "set": 
                    {
                        _tipoComando = TipoComando.SET;
                        break;
                    }

                case "run":
                    {
                        _tipoComando = TipoComando.RUN;
                        break;
                    }
                case "reset":
                    {
                        _tipoComando = TipoComando.RESET;
                        break;
                    }
                default: _tipoComando = TipoComando.SET;
                    break;
            }
        }
    }

    /// <summary>
    /// Tipos de comando possível: 
    ///  Set - Prepara o ambiente
    ///  Run - roda o teste no ambiente 
    ///  Reset - Reinicia o teste no ambiente
    /// </summary>
    public enum TipoComando
    {
        SET, RUN, RESET
    }
}
