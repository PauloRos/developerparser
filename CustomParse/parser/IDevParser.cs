﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomParse.parser
{

    public enum DevFormat { 
    
        CONSULTA_SQL, CONSULTA_VB
    
    }

    /// <summary>
    /// Parsea uma string para uma representação útil para o desenvolvedor
    /// </summary>
    public interface IDevParser
    {
        /// <summary>
        /// Retorna o texto convertido
        /// </summary>
        /// <returns> texto convertido  </returns>
        public String getText();

        public void DefinirConversao(DevFormat tipo);
        public String RetornaTextoConvertido();


    }
}
