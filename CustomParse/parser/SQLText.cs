﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace CustomParse.parser
{
    public class SQLText : IDevParser
    {
        private String _texto;
        private DevFormat TipoConversao;
        private static String Pattern = "((S|s)(E|e)(L|l)(E|e)(C|c)(T|t))+," +
                                        "((I|i)(N|n)(S|s)(E|e)(R|r)(T|t))+," +
                                        "((D|d)(E|e)(L|l)(E|e)(T|t)(E|e))+," +
                                        "((U|u)(P|p)(D|d)(A|a)(T|t)(E|e))+," +
                                        "((T|t)(A|a)(B|b)(L|l)(E|e))+," +
                                        "((V|v)(I|i)(E|e)(W|w))+," +
                                        "((F|f)(R|r)(O|o)(M|m))+";

        private SQLText()
        {

        }

        public static IDevParser Parse(String text)
        {

            ParseUtil.ValidarArgumentosParse(text);


            if (SQLText.IsValid(text))
            {
                IDevParser cv = new SQLText() { _texto = text, TipoConversao = DevFormat.CONSULTA_VB };
                return cv;
            }
            else
            {
                return null;
            }

        }

        public static bool IsValid(String text)
        {

            String[] chaves = Pattern.Split(",");

            for (int i = 0; i < chaves.Length; i++)
            {
                if (Regex.IsMatch(text, chaves[i]))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Retorna um texto SQl formatado para o VB6 
        /// </summary>
        /// <returns></returns>
        public String getTextoVB()
        {
            String atribuidor = "{0} = {1} _";

            StringBuilder sb = new StringBuilder();
            String[] linhas = _texto.Replace("\r","").Split("\n");
            int contador = 1;


            sb.AppendLine("'Texto convertido automáticamente.");
            sb.AppendLine(String.Format(atribuidor, "strSql", ""));
            foreach (String line in linhas)
            {


                if ((contador %  15) == 0 && contador != linhas.Length)
                {
                    sb.AppendLine($"\t\"{line} \" & VbCrLF  ");
                    sb.AppendLine(String.Empty);
                    sb.AppendLine(String.Format(atribuidor, "strSql", "strSql & "));
                    
                }
                else if (contador == linhas.Length)
                {
                    sb.AppendLine($"\t\"{line}  \" & VbCrLF  ");
                }
                else
                {
                    sb.AppendLine($"\t\"{line} \" & VbCrLF & _");
                }

                contador++;

            }
            return sb.ToString();


        }

        public string getText()
        {
            return _texto;
        }

        public void DefinirConversao(DevFormat tipo)
        {
            this.TipoConversao = tipo;
        }

        public string RetornaTextoConvertido()
        {

            switch (TipoConversao)
            {
                case DevFormat.CONSULTA_SQL:
                    {
                        return _texto;
                    }
                case DevFormat.CONSULTA_VB:
                    {

                        return getTextoVB();
                    }
                    
                default:
                    {
                        return getTextoVB();
                        
                    }
            }

        }
    }
}
