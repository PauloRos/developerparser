﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomParse.parser
{
    public class SetCommand : ICommand
    {
        private int _numeroProjeto;
        private List<String> _tabelas;

        /// <summary>
        /// Criar um comando set a partir de command text
        /// </summary>
        /// <param name="cmd"></param>
        public SetCommand(CommandText cmd)
        {
            _numeroProjeto = int.Parse( cmd.Parametros["prj"]);
            _tabelas = new List<string>(cmd.Parametros["tbl"].Split(","));
        }

        /// <summary>
        /// Cria e inicializa um command text
        /// </summary>
        /// <param name="numeroProjeto"></param>
        /// <param name="tabelas"></param>
        public SetCommand(int numeroProjeto, List<String> tabelas)
        {
            _numeroProjeto = numeroProjeto;
            _tabelas = tabelas;
        }

        public int NumeroProjeto { get => _numeroProjeto; set => _numeroProjeto = value; }
        public List<string> Tabelas { get => _tabelas; set => _tabelas = value; }

        void ICommand.Execute()
        {
            throw new NotImplementedException();
        }
    }
}
