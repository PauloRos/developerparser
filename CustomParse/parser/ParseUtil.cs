﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace CustomParse.parser
{
    public static class ParseUtil
    {

        static HashSet<IDevParser> parsers = new HashSet<IDevParser>();

        /// <summary>
        /// Pega o parser correto de acordo com o texto
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static IDevParser getParser(String text) {

            IDevParser parser = null;

           
            //Se o texto ja foi convertido não há pq converter novamente
            foreach (IDevParser item in parsers)
            {

                if (item.RetornaTextoConvertido().Contains(text))
                {
                    Trace.TraceInformation("Texto já existente.");
                    Trace.TraceInformation(text);
                    return null;
                }
            }

            if (VBText.IsValid(text))
            {
                parser = VBText.Parse(text);
            }
            else if (SQLText.IsValid(text))
            {
                parser = SQLText.Parse(text);

            }
            if (parser != null)
            {
                parsers.Add(parser);
            }
            
            return parser;

        }

        /// <summary>
        /// Validador padrao
        /// </summary>
        /// <param name="arg"></param>
        public static void ValidarArgumentosParse(String arg) {
            
            if (arg == null)
            {
                throw new ArgumentNullException("Posição de texto nula.");
            }
            if (String.IsNullOrEmpty(arg))
            {
                throw new ArgumentException("Não é possivel parsar um texto vazio.");
            }
        }


    }
}
