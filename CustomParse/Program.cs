#define TRACE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using CustomParse.objects;
using Microsoft.Win32;
using System.Configuration;
using System.Resources;
using System.Diagnostics;
using System.IO;

namespace CustomParse

{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (!EventLog.SourceExists("myTraceSources"))
            {
                EventLog.CreateEventSource("myTraceSources", "Parser");
            }

 


            // Write output to the file.
            Trace.Write("Test output ");

  

            Trace.TraceInformation("Processo de tracing iniciado.");
            EventLog log = new EventLog() { Source = "TraceSource" };

            log.WriteEntry("Aplica�� iniciada");

            int iniciaComWindows = int.Parse( ConfigurationManager.AppSettings.Get("INICIA_WINDOWS"));
            int instalada = int.Parse(ConfigurationManager.AppSettings.Get("CONFIGURADO"));
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);


            //N�o est� configurado para iniciar junto do windows
            if (iniciaComWindows==1 && instalada == 0)
            {
                log.WriteEntry("Setando configura��es - estado: inicializando com o windows.");
                try
                {
                    
                    config.AppSettings.Settings["CONFIGURADO"].Value =  1.ToString();
                    config.AppSettings.Settings["INICIA_WINDOWS"].Value = 1.ToString();
                    config.Save(ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection(config.AppSettings.SectionInformation.Name);

                    SetStartup(true);
                }
                catch (Exception x)
                {
                    log.WriteEntry(x.StackTrace, EventLogEntryType.Error);
                    //MessageBox.Show("N�o foi poss�vel definir a cofigura��o para iniciar com o windows. Verifique permiss�es");
                   
                }

            }
            else if (iniciaComWindows == 0 && instalada == 0)
            {
                Trace.TraceInformation("Setando configura��es - estado: n�o incializando com o windows.");
                try
                {
                    config.AppSettings.Settings["CONFIGURADO"].Value = 0.ToString();
                    config.AppSettings.Settings["INICIA_WINDOWS"].Value = 0.ToString();
                    config.Save(ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection(config.AppSettings.SectionInformation.Name);
                    SetStartup(false);
                }
                catch (Exception x)
                {
                    log.WriteEntry(x.StackTrace, EventLogEntryType.Error);
                    //MessageBox.Show("N�o foi poss�vel remover a cofigura��o para iniciar com o windows. Verifique permiss�es");

                }
            }

            log.Close();
            Trace.Flush();
            Trace.Close();
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1(new SOListener()));

            






        }

        public static void SetStartup(bool OnOff)
        {

            //Nome a ser exibido no registro ou quando Der MSCONFIG - Pode Alterar
            string appName = "CustomParse";

            //Diretorio da chave do Registro NAO ALTERAR
            string runKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";

            //Abre o registro
            RegistryKey startupKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            //Valida se vai incluir o iniciar com o Windows ou remover
            if (OnOff)//Iniciar
            {
                if (startupKey.GetValue(appName) == null)
                {
                    // Add startup reg key
                    startupKey.SetValue(appName, @"""" + Application.ExecutablePath.ToString() + @"""");
                    startupKey.Close();
                }
            }
            else//Nao iniciar mais
            {
                // remove startup
                startupKey = Registry.LocalMachine.OpenSubKey(runKey, true);
                startupKey.DeleteValue(appName, false);
                startupKey.Close();
            }
        }

    }

}
