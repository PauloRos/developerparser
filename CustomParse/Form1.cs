﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CustomParse.objects;

namespace CustomParse
{
    public partial class Form1 : Form
    {

        // Declare the ContextMenuStrip control.
        private ContextMenuStrip menuAcoes;
        private System.Windows.Forms.NotifyIcon notificador;
        private ToolStripMenuItem iniciarToolStripMenuItem;
        private ToolStripMenuItem pararToolStripMenuItem;
        private ToolStripMenuItem fecharToolStripMenuItem;

        public SOListener Parser { get; set; }


        public Form1()
        {
            InitializeComponent();
       
        }

        private void Inicializar()
        {
            menuAcoes = new ContextMenuStrip(this.components);

            // Assign the ContextMenuStrip to the form's 
            // ContextMenuStrip property.
            this.ContextMenuStrip = menuAcoes;

           
            // Create the NotifyIcon.
            this.notificador = new System.Windows.Forms.NotifyIcon(this.components);

            this.notificador.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notificador.BalloonTipText = "Leitor de texto do desenvolvedor ativado ";
            this.notificador.ContextMenuStrip = this.ContextMenuStrip;
            this.notificador.Text = "Leitura dos textos de desenvolvimento";
            this.notificador.Icon = SystemIcons.Information;
            
            // contextMenuStrip


            pararToolStripMenuItem = new ToolStripMenuItem();
            iniciarToolStripMenuItem = new ToolStripMenuItem();
            fecharToolStripMenuItem = new ToolStripMenuItem();

            this.ContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                                                this.pararToolStripMenuItem, this.iniciarToolStripMenuItem, this.fecharToolStripMenuItem});
            this.ContextMenuStrip.Name = "contextMenuStrip";
            this.ContextMenuStrip.Size = new System.Drawing.Size(153, 150);


            // iniciarToolStripMenuItem
            this.iniciarToolStripMenuItem.Name = "iniciarToolStripMenuItem";
            this.iniciarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.iniciarToolStripMenuItem.Text = "Iniciar";
            this.iniciarToolStripMenuItem.Click += new System.EventHandler(this.IniciarLeitor);

            // 
            // pararToolStripMenuItem
            // 
            this.pararToolStripMenuItem.Name = "pararToolStripMenuItem";
            this.pararToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.pararToolStripMenuItem.Text = "Parar";
            this.pararToolStripMenuItem.Click += new System.EventHandler(this.pararLeitor);


            //Fechar
            this.fecharToolStripMenuItem.Name = "fecharToolStripMenuItem";
            this.fecharToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.fecharToolStripMenuItem.Text = "Fechar aplicação";
            this.fecharToolStripMenuItem.Click += new System.EventHandler(this.FecharLeitor);

            this.WindowState = FormWindowState.Minimized;
            this.Visible = false;

        }

        protected override void OnResize(EventArgs e)
        {

            //if the form is minimized  
            //hide it from the task bar  
            //and show the system tray icon (represented by the NotifyIcon control)  
            if (this.WindowState == FormWindowState.Minimized)
            {
                Hide();
                this.notificador.Visible = true;
            }
            base.OnResize(e);
        }

   
        protected override void OnLoad(EventArgs e)
        {

            //this.notificador.Visible = true;
            this.notificador.ShowBalloonTip(1000);
            this.iniciarToolStripMenuItem.ForeColor = Color.Gray;
            

            base.OnLoad(e);
        
        }

        private void pararLeitor(object sender, EventArgs e)
        {
            this.notificador.ShowBalloonTip(1000,"Parado", "leitura de textos interrompida.", System.Windows.Forms.ToolTipIcon.Info);
            this.iniciarToolStripMenuItem.ForeColor = Color.Black;
            pararToolStripMenuItem.ForeColor = Color.Gray;
            Parser.running = false;
            
        }

        private void IniciarLeitor(object sender, EventArgs e)
        {
            this.notificador.ShowBalloonTip(1000);
            Parser.running = true;
            pararToolStripMenuItem.ForeColor = Color.Black;
            iniciarToolStripMenuItem.ForeColor = Color.Gray; 
        }

        private void FecharLeitor(object sender, EventArgs e)
        {
            pararLeitor(sender, e);
            Application.Exit();
        }

        public Form1(SOListener Parser)
        {
            this.Parser = Parser;
            Parser.RegisterClipboardViewer(this);
            InitializeComponent();
            Inicializar();

   
        }

        //Recebe a mensagem do SO e envia para o parser tratar
        protected override void WndProc(ref Message m)
        {
            if (!Parser.WndProc(m, this))
            {
                
                base.WndProc(ref m);
            }

        }

        protected override void OnClosing(CancelEventArgs e)
        {
            Parser.UnregisterClipboardViewer(this);
            this.notificador.Visible = false;
            base.OnClosing(e);
        }

   

    }
}
