﻿using System;
using System.Collections.Generic;
using System.Text;
using CustomParse.objects;
using System.Windows.Forms;
using System.Diagnostics;
using CustomParse.parser;


namespace CustomParse.objects
{



    public class SOListener
    {


        IntPtr _clipboardPointer;
        private bool myLock;
        public bool running { get; set; }


        public SOListener()
        {
            running = true;
        }

        /// <summary>
        /// Register this form as a Clipboard Viewer application
        /// </summary>
        public void RegisterClipboardViewer(Form formulario)
        {
            _clipboardPointer = NativeUsr32.SetClipboardViewer(formulario.Handle);
        }

        /// <summary>
        /// Remove this form from the Clipboard Viewer list
        /// </summary>
        public void UnregisterClipboardViewer(Form formulario)
        {
            NativeUsr32.ChangeClipboardChain(formulario.Handle, _clipboardPointer);
        }

        //Comunica com o SO para capturar uma mensagem
        public bool WndProc(Message m, Form formulario)
        {
            bool result = true;
            if (myLock)
            {
                return true;
            }
            switch ((Msgs)m.Msg)
            {
                //
                // The WM_DRAWCLIPBOARD message is sent to the first window 
                // in the clipboard viewer chain when the content of the 
                // clipboard changes. This enables a clipboard viewer 
                // window to display the new content of the clipboard. 
                //
                case Msgs.WM_DRAWCLIPBOARD:

                    Debug.WriteLine("WindowProc DRAWCLIPBOARD: " + m.Msg, "WndProc");


                    try
                    {
                        IDataObject iData = Clipboard.GetDataObject();

                        if (iData.GetDataPresent(DataFormats.Text))
                        {


                            IDevParser devParser = ParseUtil.getParser((string)iData.GetData(DataFormats.Text));
                            // UnregisterClipboardViewer(formulario);
                            myLock = false;
                            if (devParser != null)
                            {

                               
                                if (running)
                                {

                                    myLock = true;
                                    Clipboard.SetText(devParser.RetornaTextoConvertido());


                                }

                            }


                        }


                    }

                    catch (Exception ex)
                    {

                        Trace.Write(ex);
                        return false;
                    }





                    //
                    // Each window that receives the WM_DRAWCLIPBOARD message 
                    // must call the SendMessage function to pass the message 
                    // on to the next window in the clipboard viewer chain.
                    //
                    NativeUsr32.SendMessage(this._clipboardPointer, m.Msg, m.WParam, m.LParam);
                    myLock = false;
                    break;


                //
                // The WM_CHANGECBCHAIN message is sent to the first window 
                // in the clipboard viewer chain when a window is being 
                // removed from the chain. 
                //
                case Msgs.WM_CHANGECBCHAIN:
                    Debug.WriteLine("WM_CHANGECBCHAIN: lParam: " + m.LParam, "WndProc");

                    // When a clipboard viewer window receives the WM_CHANGECBCHAIN message, 
                    // it should call the SendMessage function to pass the message to the 
                    // next window in the chain, unless the next window is the window 
                    // being removed. In this case, the clipboard viewer should save 
                    // the handle specified by the lParam parameter as the next window in the chain. 

                    //
                    // wParam is the Handle to the window being removed from 
                    // the clipboard viewer chain 
                    // lParam is the Handle to the next window in the chain 
                    // following the window being removed. 
                    if (m.WParam == this._clipboardPointer)
                    {
                        //
                        // If wParam is the next clipboard viewer then it
                        // is being removed so update pointer to the next
                        // window in the clipboard chain
                        //
                        this._clipboardPointer = m.LParam;
                    }
                    else
                    {
                        NativeUsr32.SendMessage(this._clipboardPointer, m.Msg, m.WParam, m.LParam);
                    }
                    break;

                default:
                    //
                    // Let the form process the messages that we are
                    // not interested in
                    //
                    result = false;
                    break;

            }
            myLock = false;
            return result;
        }



    }
}
